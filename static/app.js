//
// Angular applicationn controller
//
angular.module('app', [])
.controller('controller', ['$scope', '$http', function ($scope, $http){

	$scope.items = [];


	// Add A new item
	$scope.add = function(name){
		$http.post('/api/v1/counter',{
			title : name
		}).then(update);
	};

	// Increment counter
	$scope.inc = function(id){
		$http.post('/api/v1/counter/inc',{
			id : id
		}).then(update);
	};

	// Decrement counter
	$scope.dec = function(id){
		$http.post('/api/v1/counter/dec',{
			id : id
		}).then(update);
	};

	// Add A new item
	$scope.del = function(id){
		$http.delete('/api/v1/counter',{
			data : {
				id : id
			}
		}).then(update);
	};
	

	function update(resp){
		$scope.items = resp.data;
		$scope.total = 0;
		angular.forEach($scope.items, function(item){
			$scope.total += item.count;
		});
	}

	// Get the items from the server
	$http.get('/api/v1/counters').then(update);
}]);